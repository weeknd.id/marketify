const FooterIcon = () => (
  <div className={`text-center text-xs mt-5`}>Designed with <a href={`https://marketify.id`} className={`font-semibold uppercase`}>Marketify</a></div>
)

export default FooterIcon