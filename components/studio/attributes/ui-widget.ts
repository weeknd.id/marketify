const UiWidget: any = {
  RADIO: { 'ui:widget': 'radio' },
  SELECT: { 'ui:widget': 'select' },
  TEXTAREA: { 'ui:widget': 'textarea' }
}

export default UiWidget