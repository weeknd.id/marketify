export const PositionSchema: any = {
  type: 'string',
  title: 'Position',
  enum: [ 'flex justify-start', 'flex justify-center', 'flex justify-end'],
  enumNames: [ 'Left', 'Center', 'Right']
}