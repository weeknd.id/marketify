import {paddingMarginSizes} from "./constant";

export const PaddingSchema: any = {
  paddingTop: {
    type: 'string',
    title: 'Padding Top',
    enum: paddingMarginSizes.map(item => `pt-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  paddingBottom: {
    type: 'string',
    title: 'Padding Bottom',
    enum: paddingMarginSizes.map(item => `pb-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  paddingLeft: {
    type: 'string',
    title: 'Padding Left',
    enum: paddingMarginSizes.map(item => `pl-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  },
  paddingRight: {
    type: 'string',
    title: 'Padding Right',
    enum: paddingMarginSizes.map(item => `pr-${item}`),
    enumNames: paddingMarginSizes.map(item => `${item}`)
  }
}

export interface IPaddingSchemaProps {
  paddingTop?: string
  paddingBottom?: string
  paddingLeft?: string
  paddingRight?: string
}

export const getPaddingClassName = ({paddingTop = '', paddingBottom = '', paddingLeft = '', paddingRight = ''}: IPaddingSchemaProps) => `${paddingTop} ${paddingBottom} ${paddingLeft} ${paddingRight}`