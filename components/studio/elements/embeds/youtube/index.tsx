import {FC} from "react";
import {PositionSchema} from "components/studio/attributes/position";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {IElementProps} from "components/studio/elements/interfaces";
import UiWidget from "components/studio/attributes/ui-widget";

interface IYoutubeProps extends IMarginSchemaProps {
  code: string
  width: number
  height: number
  position: string
}

const Youtube: FC<IElementProps> = ({ metadata, style }) => {
  const props: IYoutubeProps = metadata
  const {code, width, height, position = ''} = props
  const margin: string = getMarginClassName(props)
  const className: string = `${position} ${margin}`

  if (!code || code == '') return <>Click here to edit the youtube element attributes.</>
  return (
    <div className={className}>
      <iframe width={width ? width.toString() : "400"} height={height ? height.toString() : "200"} src={`https://www.youtube.com/embed/${code}`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen></iframe>
    </div>
  )
}

export default Youtube

export const YoutubeSchema: any = {
  form: {
    type: 'object',
    properties: {
      code: {
        type: 'string',
        title: 'Youtube Video Code'
      },
      width: {
        type: 'number',
        title: 'Width'
      },
      height: {
        type: 'number',
        title: 'Height'
      },
      position: PositionSchema,
      ...MarginSchema
    }
  },
  ui: {
    position: UiWidget.SELECT
  }
}
