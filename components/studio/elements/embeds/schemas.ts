import {ElementTypes} from "../types";
import {FacebookSchema} from "./facebook";
import {InstagramSchema} from "./instagram";
import {YoutubeSchema} from "./youtube";

const EmbedSchemas: any = (elementType: ElementTypes): any => {
  let schema: any = undefined

  switch (elementType) {
    case ElementTypes.EMBED_FACEBOOK:
      schema = FacebookSchema
      break
    case ElementTypes.EMBED_INSTAGRAM:
      schema = InstagramSchema
      break
    case ElementTypes.EMBED_YOUTUBE:
      schema = YoutubeSchema
      break
  }

  return schema
}

export default EmbedSchemas