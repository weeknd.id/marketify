import {FC} from "react";
import {PositionSchema} from "../../../attributes/position";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {IElementProps} from "components/studio/elements/interfaces";
import UiWidget from "components/studio/attributes/ui-widget";

interface IInstragramProps extends IMarginSchemaProps {
  url: string
  width: number
  height: number
  position: string
}

const Instagram: FC<IElementProps> = ({ metadata, style }) => {
  const props: IInstragramProps = metadata
  const {url, width, height, position = ''} = props
  const margin: string = getMarginClassName(props)
  const className: string = `${position} ${margin}`

  if (!url || url == '') return <>Click edit icon to edit instagram element.</>
  return (
    <div className={className}>
      <iframe
        src={`${url.replaceAll('?utm_source=ig_web_copy_link', '')}embed`}
        width={width ? width.toString() : "400"} height={height ? height.toString() : "600"} style={{border:'none', overflow:'hidden'}} scrolling="no" frameBorder="0"
        allowFullScreen={true}
        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
    </div>
  )
}

export default Instagram

export const InstagramSchema: any = {
  form: {
    type: 'object',
    properties: {
      url: {
        type: 'string',
        title: 'Instagram Post URL'
      },
      // width: {
      //   type: 'number',
      //   title: 'Width'
      // },
      // height: {
      //   type: 'number',
      //   title: 'Height'
      // },
      position: PositionSchema,
      ...MarginSchema
    }
  },
  ui: {
    position: UiWidget.SELECT
  }
}
