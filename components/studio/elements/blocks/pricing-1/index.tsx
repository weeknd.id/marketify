import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "components/studio/attributes/ga-event";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";

interface IBlockPricing1Props extends IGoogleAnalyticsEventProps {
  price: string
  perText: string
  headline: string
  description: string
  linkText: string
  linkUrl: string
}

export default function BlockPricing1({ metadata, style }: IElementProps) {
  const { price, perText, headline, description, linkText, linkUrl, gaEventName, gaEventVariables}: IBlockPricing1Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className="text-blueGray-700 ">
      <div className="container items-center px-5 py-8 mx-auto">
        <div className="flex flex-col mb-12 text-center">
          <div className="w-full mx-auto lg:w-1/3">
            <div className="p-6 mx-auto">
              <strong className="flex items-end justify-center mx-auto mb-8 text-3xl font-black leading-none text-center text-black lg:text-4xl">
                {price || `$10`}
                <span className="text-sm">{`/${perText || 'mo'}`}</span>
              </strong>
              <h1
                className={`mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter ${color ? color.getTextColor() : `text-black`} lg:text-3xl title-font`}>
                {headline || `Short length headline.`}
              </h1>
              <p className="mx-auto mb-12 text-base lading-relaxed font-medium">
                {description || `You're about to launch soon and must be 100% focused on your product. Don't loose precious days designing, coding the landing page and testing .`}
              </p>
              <a href={linkUrl || `#`}
                onClick={e => gaSendEvent({gaEventName, gaEventVariables})}
                className={`px-16 py-2 text-base font-medium text-white transition duration-500 ease-in-out transform bg-${color.getBaseColor() || 'blue'}-600 border-${color.getBaseColor() || 'blue'}-600 rounded-md hover:text-white focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-${color.getBaseColor() || 'blue'}-700`}>
                  {linkText || `Buy`}
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export const BlockPricing1Schema: any = {
  form: {
    type: 'object',
    properties: {
      price: {
        type: 'string',
        title: 'Price'
      },
      perText: {
        type: 'string',
        title: 'Per Text'
      },
      headline: {
        type: 'string',
        title: 'Headline'
      },
      description: {
        type: 'string',
        title: 'Description'
      },
      linkText: {
        type: 'string',
        title: 'Link Text'
      },
      linkUrl: {
        type: 'string',
        title: 'Link URL'
      },
      ...GoogleAnalyticsEventSchema
    }
  },
  ui: {

  }
}