import {IElementProps} from "../interfaces";
import {FC} from "react";
import {ElementTypes} from "../types";

import BlockCTA1 from "./cta-1";
import BlockCTA2 from "./cta-2";

import BlockHero1 from "./hero-1";
import BlockHero2 from "./hero-2";
import BlockHero3 from "./hero-3";
import BlockHero4 from "./hero-4";
import BlockTestimonial1 from "./testimonial-1";
import BlockPricing1 from "./pricing-1";

const BlockGenerator: FC<IElementProps> = (props) => {
  let element: any

  switch (props.type) {
    case ElementTypes.BLOCK_CTA_1:
      element = <BlockCTA1 {...props} />
      break
    case ElementTypes.BLOCK_CTA_2:
      element = <BlockCTA2 {...props} />
      break

    case ElementTypes.BLOCK_HERO_1:
      element = <BlockHero1 {...props} />
      break
    case ElementTypes.BLOCK_HERO_2:
      element = <BlockHero2 {...props} />
      break
    case ElementTypes.BLOCK_HERO_3:
      element = <BlockHero3 {...props} />
      break
    case ElementTypes.BLOCK_HERO_4:
      element = <BlockHero4 {...props} />
      break

    case ElementTypes.BLOCK_PRICING_1:
      element = <BlockPricing1 {...props} />
      break

    case ElementTypes.BLOCK_TESTIMONIAL_1:
      element = <BlockTestimonial1 {...props} />
      break
  }

  return element
}

export default BlockGenerator