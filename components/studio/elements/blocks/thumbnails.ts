import {ElementTypes} from "../types";

const BlockThumbnails: any = [
  {
    title: 'CTA',
    childs: [
      { type: ElementTypes.BLOCK_CTA_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-cta-1.png' },
      { type: ElementTypes.BLOCK_CTA_2, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-cta-2.png' },
    ]
  },
  {
    title: 'Hero',
    childs: [
      { type: ElementTypes.BLOCK_HERO_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-1.png' },
      { type: ElementTypes.BLOCK_HERO_2, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-2.png' },
      { type: ElementTypes.BLOCK_HERO_3, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-3.png' },
      { type: ElementTypes.BLOCK_HERO_4, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-hero-4.png' }
    ]
  },
  {
    title: 'Pricing',
    childs: [
      { type: ElementTypes.BLOCK_PRICING_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-pricing-1.png' }
    ]
  },
  {
    title: 'Testimonial',
    childs: [
      { type: ElementTypes.BLOCK_TESTIMONIAL_1, thumbnailUrl: 'https://cosmiq.sgp1.digitaloceanspaces.com/marketify/assets/thumbnails/block-testimonial-1.png' }
    ]
  }
]

export default BlockThumbnails