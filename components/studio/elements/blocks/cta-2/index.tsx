import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "components/studio/attributes/ga-event";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IBlockCTA2Props extends IGoogleAnalyticsEventProps {
  title: string
  description: string
  buttonText: string
  buttonUrl: string
}

export default function BlockCTA2({ metadata, style }: IElementProps) {
  const { title, description, buttonText, buttonUrl, gaEventName, gaEventVariables}: IBlockCTA2Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className={`py-8 leading-7 text-gray-900 bg-white sm:py-12 md:py-16 lg:py-24`}>
      <div className={`max-w-6xl px-4 px-10 mx-auto border-solid lg:px-12`}>
        <div className={`flex flex-col items-start leading-7 text-gray-900 border-0 border-gray-200 lg:items-center lg:flex-row`}>
          <div className={`box-border flex-1 text-center border-solid sm:text-left`}>
            <h2 className={`m-0 mb-4 text-4xl font-semibold leading-tight tracking-tight text-left text-black border-0 border-gray-200 sm:text-5xl ${color.getTextColor() || 'text-indigo-600'}`}>
              {title || `Boost Your Productivity`}
            </h2>
            <p className={`mt-2 text-xl text-left text-gray-900 border-0 border-gray-200 sm:text-2xl`}>
              {description || `Our service will help you maximize and boost your productivity.`}
            </p>
          </div>
          <a href={buttonUrl || `#`}
             onClick={e => gaSendEvent({gaEventName, gaEventVariables})} className={`hover:text-${color.getBaseColor() || 'indigo'}-200 inline-flex items-center justify-center w-full px-5 py-4 mt-6 ml-0 font-sans text-base leading-none text-white no-underline bg-${color.getBaseColor() || 'indigo'}-600 border border-${color.getBaseColor() || 'indigo'}-600 border-solid rounded cursor-pointer md:w-auto lg:mt-0 hover:bg-${color.getBaseColor() || 'indigo'}-700 hover:border-${color.getBaseColor() || 'indigo'}-700 hover:text-white focus-within:bg-${color.getBaseColor() || 'indigo'}-700 focus-within:border-${color.getBaseColor() || 'indigo'}-700 focus-within:text-white sm:text-lg lg:ml-6 md:text-xl`}>
            {buttonText || `Get Started`}
            <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5 ml-2" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
              <line x1="5" y1="12" x2="19" y2="12"></line>
              <polyline points="12 5 19 12 12 19"></polyline>
            </svg>
          </a>
        </div>
      </div>
    </section>
  )
}

export const BlockCTA2Schema: any = {
  form: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
        title: 'Title Text'
      },
      description: {
        type: 'string',
        title: 'Description Text'
      },
      buttonText: {
        type: 'string',
        title: 'Button Text'
      },
      buttonUrl: {
        type: 'string',
        title: 'Button Target URL'
      },
      ...GoogleAnalyticsEventSchema
    }
  },
  ui: {
    type: UiWidget.SELECT,
    description: UiWidget.TEXTAREA
  }
}