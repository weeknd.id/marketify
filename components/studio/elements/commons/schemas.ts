import {ElementTypes} from "../types";
import {HeadingSchema} from "./heading";
import {LinkSchema} from "./link";
import {ParagraphSchema} from "./paragraph";
import {QuoteSchema} from "./quote";
import {ImageSchema} from "./image";

const CommonSchemas: any = (elementType: ElementTypes): any => {
  let schema: any = undefined

  switch (elementType) {
    case ElementTypes.HEADING:
      schema = HeadingSchema
      break
    case ElementTypes.IMAGE:
      schema = ImageSchema
      break
    case ElementTypes.LINK:
      schema = LinkSchema
      break
    case ElementTypes.LIST:
      schema = {}
      break
    case ElementTypes.PARAGRAPH:
      schema = ParagraphSchema
      break
    case ElementTypes.QUOTE:
      schema = QuoteSchema
      break
  }

  return schema
}

export default CommonSchemas