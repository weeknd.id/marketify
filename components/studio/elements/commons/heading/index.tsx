import {FC} from "react";
import {TextAlignSchema} from "components/studio/attributes/text";
import {FontSizeSchema, FontWeightSchema} from "components/studio/attributes/font";
import {getPaddingClassName, IPaddingSchemaProps, PaddingSchema} from "components/studio/attributes/padding";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IHeadingProps extends IPaddingSchemaProps, IMarginSchemaProps {
  headingType: string
  text: string
  textAlign?: string
  fontSize?: string
  fontWeight?: string
}

const Heading: FC<IElementProps> = ({ metadata, style }) => {
  const props: IHeadingProps = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  const padding: string = getPaddingClassName(props)
  const margin: string = getMarginClassName(props)
  const textColor: string = color.getTextColor()
  const { textAlign, fontSize, fontWeight } = props

  const className: string = `${textAlign} ${fontSize} ${fontWeight} ${padding} ${margin} ${textColor}`

  let heading: any
  switch (props.headingType) {
    case 'H2':
      heading = <h2 className={className}>{props.text}</h2>
      break
    case 'H3':
      heading = <h3 className={className}>{props.text}</h3>
      break
    case 'H4':
      heading = <h4 className={className}>{props.text}</h4>
      break
    case 'H5':
      heading = <h5 className={className}>{props.text}</h5>
      break
    default:
      heading = <h1 className={className}>{props.text}</h1>
  }

  if (!props.text || props.text == '') return <div className={`text-center`}>Click here to edit the heading element attributes.</div>
  return heading
}

export default Heading

export const HeadingSchema: any = {
  form: {
    type: 'object',
    properties: {
      headingType: {
        type: 'string',
        title: 'Type',
        enum: [ 'H1', 'H2', 'H3', 'H4', 'H5', 'H6' ]
      },
      text: {
        type: 'string',
        title: 'Text'
      },
      textAlign: TextAlignSchema,
      fontSize: FontSizeSchema,
      fontWeight: FontWeightSchema,
      ...PaddingSchema,
      ...MarginSchema
    }
  },
  ui: {
    textAlign: UiWidget.SELECT,
    fontSize: UiWidget.SELECT,
    fontWeight: UiWidget.SELECT
  }
}
