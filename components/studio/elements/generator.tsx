import {FC} from "react";
import {IElementProps} from "./interfaces";
import {ElementTypes} from "./types";
import Heading from "./commons/heading";
import Image from "./commons/image";
import Link from "./commons/link";
import Paragraph from "./commons/paragraph";
import Facebook from "./embeds/facebook";
import Instagram from "./embeds/instagram";
import Youtube from "./embeds/youtube";
import Quote from "./commons/quote";
import BlockGenerator from "./blocks/generator";

const ElementGenerator: FC<IElementProps> = (props) => {
  let element: any
  const {type} = props

  if (type.indexOf('BLOCK') > -1) {
    element = BlockGenerator(props)
  } else {
    switch (type) {
      case ElementTypes.HEADING:
        element = <Heading {...props} />
        break
      case ElementTypes.IMAGE:
        element = <Image {...props} />
        break
      case ElementTypes.LINK:
        element = <Link {...props} />
        break
      case ElementTypes.LIST:
        break
      case ElementTypes.PARAGRAPH:
        element = <Paragraph {...props} />
        break
      case ElementTypes.QUOTE:
        element = <Quote {...props} />
        break

      case ElementTypes.EMBED_FACEBOOK:
        element = <Facebook {...props} />
        break
      case ElementTypes.EMBED_INSTAGRAM:
        element = <Instagram {...props} />
        break
      case ElementTypes.EMBED_YOUTUBE:
        element = <Youtube {...props} />
        break

      default:
        element = <>Unknown element.</>
    }
  }

  return (
    <>{element}</>
  )
}

export default ElementGenerator