import {FC, useEffect, useState} from "react";
import {
  closestCenter,
  DndContext,
  DragEndEvent,
  KeyboardSensor,
  PointerSensor,
  TouchSensor,
  useSensor,
  useSensors
} from "@dnd-kit/core";
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  useSortable,
  verticalListSortingStrategy
} from "@dnd-kit/sortable";
import {CSS} from "@dnd-kit/utilities";
import {IElementProps} from "./elements/interfaces";
import {ElementGenerator, ElementThumbnails} from "./elements";
import Head from "next/head";
import Loading from "components/icon/loader";
import {Button, Drawer, Form, Input, Modal, notification, Popover} from "antd";
import {ElementTypes} from "./elements/types";
import EditAttributes from "./edit-attributes";
import {v1 as uuidv1} from "uuid";
import Fetch from "util/fetch";
import {useForm} from "antd/lib/form/Form";
import FooterIcon from "components/icon/footer";

interface IStudioEditorProps {
  title: string
  baseUrl?: string
  metadata?: any
  childElements: Array<IElementProps>
}

const StudioEditor: FC<IStudioEditorProps> = ({ title, baseUrl = '', metadata = {}, childElements= [] }) => {
  const [pageHeight, setPageHeight] = useState<number>(0)
  const [isPageLoading, setIsPageLoading] = useState<boolean>(false)

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(TouchSensor),
    useSensor(KeyboardSensor, { coordinateGetter: sortableKeyboardCoordinates })
  )

  const [elements, setElements] = useState<Array<IElementProps>>([])
  const [editElementIndex, setEditElementIndex] = useState<number>(-1)

  const onDragEnd = (e: DragEndEvent) => {
    const {active, over} = e

    if (active.id !== over!.id) {
      const oldIndex = elements.map(elem => elem.id).indexOf(active.id)
      const newIndex = elements.map(elem => elem.id).indexOf(over!.id)
      setElements(arrayMove(elements, oldIndex, newIndex))
    }
  }

  const onClickEdit = (flag: boolean, index: number) => {
    if (flag) setEditElementIndex(index)
    else setEditElementIndex(-1)
  }

  const onClickRemove = (index: number) => {
    const tempElements = JSON.parse(JSON.stringify(elements))
    tempElements.splice(index, 1)
    setElements(tempElements)
  }

  const onClickDuplicate = (index: number) => {
    const origElements = JSON.parse(JSON.stringify(elements))
    const newElement = {...origElements[index], id: uuidv1()}
    setElements([...origElements, newElement])
  }

  const onSave = async (opts: any, setIsLoading: Function, successMsg: string) => {
    try {
      setIsLoading(true)
      const result: any = await Fetch('PUT', `/api/v1/page/${metadata.id}`, opts)
      if (!result.success) {
        notification.error({ message: 'Gagal Melakukan Update', description: result.error })
      } else {
        localStorage.setItem('draft-elements', JSON.stringify(elements))
        notification.success({ message: 'Sukses', description: successMsg })
      }
    } catch (e: any) {
      console.dir(e)
      notification.error({ message: 'Terjadi kesalahan', description: 'Coba lagi ya nanti.' })
    } finally {
      setIsLoading(false)
    }
  }

  const [isLoadingSaveDraft, setIsLoadingSaveDraft] = useState<boolean>(false)
  const onSaveAsDraft = async () => {
    if (isLoadingSaveDraft) return
    const opts: any = {
      type: 'element-draft',
      elementDraft: elements
    }
    await onSave(opts, setIsLoadingSaveDraft, 'Draft berhasil disimpan.')
  }

  const [isLoadingSavePublished, setIsLoadingSavePublished] = useState<boolean>(false)
  const onPublish = async () => {
    if (isLoadingSaveDraft) return
    const opts: any = {
      type: 'element-published',
      elementPublished: elements
    }
    await onSave(opts, setIsLoadingSavePublished, 'Page berhasil disimpan dan dipublikasikan.')
  }

  const [isShowElementDrawer, setIsShowElementDrawer] = useState<boolean>(false)
  const [elementDrawer, setElementDrawer] = useState<ElementTypes>(ElementTypes.UNKNOWN)

  const [isShowPageSettingModal, setIsShowPageSettingModal] = useState<boolean>(false)

  const [formPage] = useForm()
  const [isLoadingSavePageSettings, setIsLoadingSavePageSettings] = useState<boolean>(false)
  const onSavePageSettings = async () => {
    if (isLoadingSavePageSettings) return
    try {
      setIsLoadingSavePageSettings(true)
      const opts: any = {
        type: 'page',
        ...JSON.parse(JSON.stringify(formPage.getFieldsValue())),
        metadata: {
          googleAnalyticsTrackingCode: formPage.getFieldValue('googleAnalyticsTrackingCode')
        }
      }
      delete opts.googleAnalyticsTrackingCode
      const result: any = await Fetch('PUT', `/api/v1/page/${metadata.id}`, opts)
      if (!result.success) {
        notification.error({ message: 'Gagal Melakukan Update', description: result.error })
      } else {
        localStorage.setItem('draft-elements', JSON.stringify(elements))
        notification.success({ message: 'Sukses', description: 'Detil Page berhasil disimpan.' })
      }
    } catch (e) {
      console.dir(e)
      notification.success({ message: 'Terjadi Kesalahan', description: 'Coba kembali beberapa saat lagi.' })
    } finally {
      setIsLoadingSavePageSettings(false)
    }
  }

  const onClickMoveUp = (index: number) => {
    setElements(arrayMove(elements, index, index-1))
  }

  const onClickMoveDown = (index: number) => {
    setElements(arrayMove(elements, index, index+1))
  }

  useEffect(() => {
    // if (process.browser) setPageHeight(window.innerHeight - 50)
    setElements(childElements)
    metadata.googleAnalyticsTrackingCode = metadata.metadata.googleAnalyticsTrackingCode
    formPage.setFieldsValue(metadata)
  }, [])

  return (
    <div>
      <Head>
        <title>{title.toUpperCase()} Studio</title>
        <link ref={`stylesheet`} href={`/assets/css/studio.css`} />
      </Head>
      <main>
        {isPageLoading &&
          <div className={`h-screen flex justify-center items-center`}>
            <Loading />
          </div>
        }

        {!isPageLoading &&
          <>
            <div className={`w-full fixed flex justify-between items-center bg-indigo-600 p-3 px-5 md:px-3`}>
              <div>
                <a href={'/dashboard'} className={`text-white uppercase tracking-wider text-xs`}><span className={`font-bold`}>{title}</span> Studio</a>
              </div>
              <div className={`block md:hidden`}>
                <Popover placement={`bottomRight`} trigger={`click`}
                content={<div className={`space-y-3`}>
                  <a className={`block text-indigo-600 hover:text-indigo-300`} onClick={e => setIsShowPageSettingModal(true)}>Pengaturan</a>
                  <a className={`block text-indigo-600 hover:text-indigo-300`} href={`/studio/preview`} target={`_blank`}>Pratinjau</a>
                  <hr />
                  <a className={`block text-indigo-600 hover:text-indigo-300`} onClick={onSaveAsDraft}>
                    {isLoadingSaveDraft ? 'Menyimpan ...' : 'Simpan Draft'}
                  </a>
                  <a className={`block text-indigo-600 hover:text-indigo-300`} onClick={onPublish}>
                    {isLoadingSavePublished ? 'Mempublikasikan ...' : 'Publikasi'}
                  </a>
                  <a className={`block text-indigo-600 hover:text-indigo-300`}
                     onClick={e => {
                       navigator.clipboard.writeText(`${baseUrl}/${metadata.slug}`)
                       notification.success({ message: 'Sukses', description: 'Link berhasil di copy.' })
                     }}>Share Link</a>
                </div>}>
                  <i className={`fa fa-bars text-white cursor-pointer`} />
                </Popover>
              </div>
              <div className={`hidden md:block`}>
                <div className={`space-x-3 text-xs flex items-center`}>
                  <div className={`space-x-5 mr-5`}>
                    <a className={`text-white hover:text-indigo-300`} onClick={e => setIsShowPageSettingModal(true)}>Pengaturan</a>
                    <a className={`text-white hover:text-indigo-300`} href={`/studio/preview`} target={`_blank`}>Pratinjau</a>
                  </div>
                  <a className={`bg-white border border-white text-indigo-600 hover:text-indigo-300 rounded px-5 py-1`} onClick={onSaveAsDraft}>
                    {isLoadingSaveDraft ? 'Menyimpan ...' : 'Simpan Draft'}
                  </a>
                  <a className={`border border-white text-white rounded px-5 py-1 hover:text-indigo-300`} onClick={onPublish}>
                    {isLoadingSavePublished ? 'Mempublikasikan ...' : 'Publikasi'}
                  </a>
                  <a className={`border border-white text-white rounded px-5 py-1 hover:text-indigo-300`}
                     onClick={e => {
                       navigator.clipboard.writeText(`${baseUrl}/${metadata.slug}`)
                       notification.success({ message: 'Sukses', description: 'Link berhasil di copy.' })
                     }}>Share Link</a>
                </div>
              </div>
            </div>

            <div className={`flex flex-col justify-center`}>
              {/*<div className={`w-2/12 left-0 fixed bg-gray-100 border-r border-gray-200`} style={{height: pageHeight, marginTop: 50}}>*/}
              {/*  <ElementThumbnails elements={elements} setElements={setElements} />*/}
              {/*</div>*/}

              <div className={`w-full pt-20 px-5`}>
                <DndContext
                  sensors={sensors}
                  collisionDetection={closestCenter}
                  onDragEnd={onDragEnd}>
                  <SortableContext strategy={verticalListSortingStrategy} items={elements.map(el => el.id)}>
                    {elements.map((elem, i) => (
                      <SortableItem key={i} index={i} maxIndex={elements.length-1} {...elem}
                        onClickEdit={onClickEdit}
                        onClickRemove={onClickRemove}
                        onClickDuplicate={onClickDuplicate}
                        onClickMoveUp={onClickMoveUp}
                        onClickMoveDown={onClickMoveDown}>
                          <ElementGenerator {...elem} />
                      </SortableItem>
                    ))}
                  </SortableContext>
                </DndContext>
              </div>

              <div className={`flex justify-center my-10`}>
                <div className={`space-x-4`}>
                  <Button type={`primary`} onClick={e => {
                    setIsShowElementDrawer(true)
                    setElementDrawer(ElementTypes.COMMON)
                  }}>+ Common</Button>

                  <Button type={`primary`} onClick={e => {
                    setIsShowElementDrawer(true)
                    setElementDrawer(ElementTypes.EMBED)
                  }}>+ Embed</Button>

                  <Button type={`primary`} onClick={e => {
                    setIsShowElementDrawer(true)
                    setElementDrawer(ElementTypes.BLOCK)
                  }}>+ Block</Button>
                </div>
              </div>

              <FooterIcon />

              {/*<div className={`w-2/12 right-0 fixed overflow-y-auto bg-gray-100 border-l border-gray-200`} style={{height: pageHeight, marginTop: 50}}>*/}
              {/*  <EditAttributes*/}
              {/*    elements={elements}*/}
              {/*    setElements={setElements}*/}
              {/*    editElementIndex={editElementIndex}*/}
              {/*    setEditElementIndex={setEditElementIndex} />*/}
              {/*</div>*/}
            </div>
          </>
        }
      </main>

      <Drawer visible={isShowElementDrawer} onClose={e => { setIsShowElementDrawer(false)}} placement={`bottom`} height={400}>
        <ElementThumbnails type={elementDrawer} elements={elements} setElements={setElements} />
      </Drawer>

      <Drawer visible={editElementIndex > -1 ? true : false} onClose={e => { setEditElementIndex(-1)}} placement={`right`} width={300}>
        <EditAttributes
            elements={elements}
            setElements={setElements}
            editElementIndex={editElementIndex}
            setEditElementIndex={setEditElementIndex} />
      </Drawer>

      <Modal title={`Pengaturan`} visible={isShowPageSettingModal} onCancel={e => {
        if(!isLoadingSavePageSettings) setIsShowPageSettingModal(false)
      }} footer={null}>
        <Form layout={`vertical`} form={formPage} onFinish={onSavePageSettings}>
          <Form.Item label={`Judul`} name={`title`}><Input /></Form.Item>
          <Form.Item label={`Slug untuk Share Link`} name={`slug`}><Input addonBefore={`${baseUrl}/`} defaultValue={`my-page`} /></Form.Item>
          <Form.Item label={`Deskripsi`} name={`description`}><Input /></Form.Item>
          <Form.Item label={`Kode Tracking Google Analytics`} name={`googleAnalyticsTrackingCode`}><Input placeholder={`Contoh U-xxxx atau G-xxxx`} /></Form.Item>
          <div className={`flex justify-end`}>
            <Button type={`primary`} htmlType={`submit`}>{isLoadingSavePageSettings ? 'Menyimpan ... ' : 'Simpan'}</Button>
          </div>
        </Form>
      </Modal>
    </div>
  )
}

const SortableItem = (props: any) => {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
  } = useSortable({id: props.id})

  const style: any = {
    transform: transform ? CSS.Transform.toString({ x: transform.x, y: transform.y, scaleY: 1, scaleX: 1}) : CSS.Transform.toString(null),
    transition,
  }

  return (
    <div ref={setNodeRef} style={style} className={`bg-white element block w-full group border border-transparent hover:border-indigo-600`}>
      <div className={`w-full`}>{props.children}</div>
      <div className={`w-full element-control pb-3`}>
        <div className={`flex justify-center`}>
          <div className={`flex space-x-3`}>
            <div className={`border hover:border-indigo-600 hover:bg-indigo-600 hover:text-white w-10 h-10 rounded-full flex justify-center items-center cursor-pointer`} onClick={e => props.onClickEdit(true, props.index)} >
              <i className={`fa fa-edit text-lg`} />
            </div>
            <div className={`border hover:border-indigo-600 hover:bg-indigo-600 hover:text-white w-10 h-10 rounded-full flex justify-center items-center cursor-pointer`} onClick={e => props.onClickDuplicate(props.index)} >
              <i className={`fa fa-copy text-lg`} />
            </div>
            <div className={`border hover:border-indigo-600 hover:bg-indigo-600 w-10 h-10 rounded-full flex justify-center items-center cursor-pointer`} onClick={e => props.onClickRemove(props.index)} >
              <i className={`fa fa-remove text-red-500 text-lg`} />
            </div>
            <div className={`hidden md:block border hover:border-indigo-600 hover:text-white hover:bg-indigo-600 w-10 h-10 rounded-full md:flex md:justify-center md:items-center cursor-move`} data-cypress="draggable-handle" {...attributes} {...listeners}>
              <i className={`fa fa-arrows text-lg`} />
            </div>
            {props.index != 0 &&
              <div className={`block md:hidden border hover:border-indigo-600 w-10 h-10 rounded-full flex justify-center items-center cursor-pointer`} onClick={e => props.onClickMoveUp(props.index)}>
                <i className={`fa fa-arrow-up text-lg`} />
              </div>
            }
            {props.index != props.maxIndex &&
              <div className={`block md:hidden border hover:border-indigo-600 w-10 h-10 rounded-full flex justify-center items-center cursor-pointer`} onClick={e => props.onClickMoveDown(props.index)}>
                <i className={`fa fa-arrow-down text-lg`} />
              </div>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default StudioEditor