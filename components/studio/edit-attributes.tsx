import {Tabs} from "antd";
import {IElementProps} from "./elements/interfaces";
import {FC, useEffect, useState} from "react";
import {ElementTypes} from "./elements/types";
import ElementForms from "./elements/forms";
import ColorAttributes from "./attributes/color";

interface IEditAttributesProps {
  elements: Array<IElementProps>
  setElements: Function
  editElementIndex?: number
  setEditElementIndex: Function
}

const EditAttributes: FC<IEditAttributesProps> = ({ elements, setElements, editElementIndex = -1, setEditElementIndex }) => {
  // @ts-ignore
  const [element, setElement] = useState<IElementProps>({ type: ElementTypes.UNKNOWN })
  const [elementMetadata, setElementMetadata] = useState<any>({})
  const [elementStyle, setElementStyle] = useState<any>({})

  const onChangeEditElementMetadata = (e: any) => {
    const metadata: any = {...elementMetadata, ...e.formData}
    setElementMetadata(metadata)
    onApplyChanges(metadata, elementStyle)
  }

  const onChangeEditElementStyle = (style: any) => {
    let tempStyle = JSON.parse(JSON.stringify(elementStyle))
    tempStyle[style.type] = style.data

    const newStyle = {...elementStyle, ...tempStyle}
    setElementStyle(newStyle)
    onApplyChanges(elementMetadata, newStyle)
  }

  const onChangeColorAttributes = (primaryColor: any) =>
    onChangeEditElementStyle({ type: 'primaryColor', data: primaryColor })

  const onApplyChanges = (metadata: any = undefined, style: any = undefined) => {
    let el = JSON.parse(JSON.stringify(element))
    el.metadata = metadata || elementMetadata
    el.style = style || elementStyle

    let tempElements = JSON.parse(JSON.stringify(elements))
    tempElements[editElementIndex] = el
    setElements(tempElements)
  }

  useEffect(() => {
    if(elements[editElementIndex]) {
      const el: IElementProps = JSON.parse(JSON.stringify(elements[editElementIndex]))
      setElement(el)
      setElementMetadata(el.metadata)
      setElementStyle(el.style)
    } else {
      setElement({ id: `unknown`, type: ElementTypes.UNKNOWN, metadata: {}, style: {}})
      setElementMetadata({})
      setElementStyle({})
    }
  }, [editElementIndex])

  return (
    <div id={`studio-side-nav-right`}>
      {editElementIndex == -1 && <div className={`p-3`}>Please select an element to edit element attributes.</div>}
      {editElementIndex > -1 &&
      <>
        <Tabs defaultActiveKey={`1`}>
          <Tabs.TabPane tab={`Element`} key={`1`}>
            <ElementForms id={`element`} type={element.type} metadata={elementMetadata} style={elementStyle} onChangeEdit={onChangeEditElementMetadata} />
          </Tabs.TabPane>
          <Tabs.TabPane tab={`Primary Color`} key={`2`}>
            <ColorAttributes onChange={onChangeColorAttributes}/>
          </Tabs.TabPane>
        </Tabs>
      </>
      }
    </div>
  )
}

export default EditAttributes