import {SUBSCRIPTION_TYPES} from "repositories/schemas";
import moment from "moment";

export default class SubscriptionService {
  constructor() {}

  public getMaxTotalCreatedPage(type: SUBSCRIPTION_TYPES) {
    let max: number = 0

    switch (type) {
      case SUBSCRIPTION_TYPES.FREE_TRIAL:
        max = 1
        break
      case SUBSCRIPTION_TYPES.STARTER:
        max = 5
        break
      case SUBSCRIPTION_TYPES.PRO:
        max = 20
    }

    return max
  }

  public generateExpiredDate(type: SUBSCRIPTION_TYPES) {
    let expiredDate: any = moment().format(`YYYY-MM-DD HH:mm:ss`)

    switch (type) {
      case SUBSCRIPTION_TYPES.FREE_TRIAL:
        expiredDate = moment().add(9, 'days').format(`YYYY-MM-DD HH:mm:ss`)
        break
      case SUBSCRIPTION_TYPES.STARTER:
        expiredDate = moment().add(30, 'days').format(`YYYY-MM-DD HH:mm:ss`)
        break
      case SUBSCRIPTION_TYPES.PRO:
        expiredDate = moment().add(30, 'days').format(`YYYY-MM-DD HH:mm:ss`)
    }

    return expiredDate
  }
}