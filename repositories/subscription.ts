import {ISubscription, ISubscriptionHistory, SUBSCRIPTION_TYPES} from "./schemas";
import {v1 as uuidv1} from "uuid";
import dbExec from "./index";

export default class SubscriptionRepository {
  constructor() { }

  public async create(subscription: ISubscription) {
    subscription.id = uuidv1()
    subscription.updatedAt = new Date()

    const query = `mutation subscriptionCreate(
      $id: String,
      $accountId: String,
      $type: String,
      $metadata: jsonb = {},
      $expiredAt: timestamp = "",
      $updatedAt: timestamp = ""
    ) {
      insert_subscription_one(object: {
        id: $id,
        accountId: $accountId,
        type: $type,
        metadata: $metadata,
        expiredAt: $expiredAt,
        updatedAt: $updatedAt
      }) {
        id
        accountId
        type
        expiredAt
      }
    }`

    return await dbExec(query, subscription)
  }

  public async createHistory(subscriptionHistory: ISubscriptionHistory) {
    subscriptionHistory.id = uuidv1()
    subscriptionHistory.updatedAt = new Date()

    const query = `mutation subscriptionHistoryCreate(
      $id: String,
      $subscriptionId: String,
      $type: String,
      $currency: String,
      $price: bigint,
      $metadata: jsonb = {},
      $expiredAt: timestamp = "",
      $updatedAt: timestamp = ""
    ) {
      insert_subscription_history_one(object: {
        id: $id,
        subscriptionId: $subscriptionId,
        type: $type,
        currency: $currency,
        price: $price,
        metadata: $metadata,
        expiredAt: $expiredAt,
        updatedAt: $updatedAt
      }) {
        id
      }
    }`

    return await dbExec(query, subscriptionHistory)
  }
}