import Head from "next/head";
import Loading from "components/icon/loader";

export default function Layout({ title, isLoading, children }: any) {
  const gaMeasurementId: string = ""

  return (
    <div>
      <Head>
        <title>{title}</title>
        <script async src={`https://www.googletagmanager.com/gtag/js?id=${gaMeasurementId}`} />
        <script
          dangerouslySetInnerHTML={{
            __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '${gaMeasurementId}', {
                  page_path: window.location.pathname,
                });
              `,
          }}></script>
      </Head>
      <main>
        {isLoading ? (
          <div className={`w-full h-screen flex justify-center items-center`}>
            <Loading />
          </div>
        ):(
          <>
            <div className={`w-full bg-indigo-600 p-3 px-5 md:px-3`}>
              <div className={`container mx-auto flex justify-between items-center`}>
                <div className={`text-white uppercase tracking-wider text-md`}><a href={`/dashboard`} className={`text-white font-bold`}>Marketify</a></div>
                <div>
                  <div className={`space-x-6 text-xs flex items-center uppercase text-xs tracking-wider`}>
                    <a href={`/trending-keyword`} className={`text-white hover:text-indigo-300`}>Trending Keyword</a>
                    {/* <a href={`/settings`} className={`text-white hover:text-indigo-300`}>Settings</a> */}
                    <a href={`/api/oauth/logout`} className={`text-white hover:text-indigo-300`}>Log Out</a>
                  </div>
                </div>
              </div>
            </div>

            {children}
          </>
        )}
      </main>
    </div>
  )
}