import type {NextApiRequest, NextApiResponse} from "next";
import constant, {cookieKey} from "common/constant";
import Fetch from "util/fetch";
import {v1 as uuidv1} from "uuid";
import {setCookie} from "nookies";
import AccountRepository from "repositories/account";
import SubscriptionRepository from "repositories/subscription";
import {ISubscription, ISubscriptionHistory, SUBSCRIPTION_TYPES} from "repositories/schemas";
import jwt from "jsonwebtoken";
import SubscriptionService from "services/subscription";

const OauthProviderCallback = async (req: NextApiRequest, res: NextApiResponse) => {
  const { code, provider } = req.query
  let account: any = {}

  if (provider === "google") {
    const clientId = process.env.GOOGLE_CLIENT_ID
    const clientSecret = process.env.GOOGLE_CLIENT_SECRET
    const redirectUri = process.env.GOOGLE_REDIRECT_URI

    let url = `${constant.GoogleTokenUrl}?client_id=${clientId}&client_secret=${clientSecret}&redirect_uri=${redirectUri}&code=${code}&grant_type=authorization_code`
    let resp = await Fetch("POST", url)
    if (!resp.access_token) {
      return res.redirect("/error?code=login")
    }

    url = `${constant.GoogleApiUrl}/v3/userinfo?alt=json&access_token=${resp.access_token}`
    resp = await Fetch("POST", url)
    if (!resp.email) {
      return res.redirect("/error?code=login")
    }

    account = {
      googleId: resp.sub,
      email: resp.email,
      fullName: resp.name,
      picUrl: resp.picture,
      birthday: null
    }
  } else if (provider === "facebook") {
    const clientId = process.env.FACEBOOK_CLIENT_ID
    const clientSecret = process.env.FACEBOOK_CLIENT_SECRET
    const redirectUri = process.env.FACEBOOK_REDIRECT_URI
    const fields = process.env.FACEBOOK_OAUTH_FIELDS

    let url = `${constant.FacebookGraphUrl}/oauth/access_token?client_id=${clientId}&client_secret=${clientSecret}&redirect_uri=${redirectUri}&code=${code}&grant_type=authorization_code`
    let resp = await Fetch("GET", url)
    if (!resp.access_token) {
      return res.redirect("/error?code=login")
    }

    url = `${constant.FacebookGraphUrl}/me?fields=${fields}&access_token=${resp.access_token}`
    resp = await Fetch("GET", url)
    if (!resp.email) {
      return res.redirect("/error?code=login")
    }

    account = {
      facebookId: resp.id,
      email: resp.email,
      fullName: `${resp.first_name} ${resp.last_name}`,
      picUrl: resp.picture.data.url || null,
      birthday: resp.birthday || null
    }
  }

  account = {
    ...account,
    id: uuidv1(),
    username: account.email.split('@')[0]
  }
  delete account.googleId
  delete account.facebookId
  delete account.birthday

  const accountRepository: AccountRepository = new AccountRepository()
  const subscriptionRepository: SubscriptionRepository = new SubscriptionRepository()
  const subscriptionService: SubscriptionService = new SubscriptionService()

  let result: any = {}

  result = await accountRepository.findByEmail(account.email)
  if (result.errors) return res.redirect('/error/500?id=find-account')
  if (result.data.account && result.data.account.length < 1) {
    result = await new AccountRepository().create(account)
    if (result.errors) return res.redirect('/error/500?id=create-account')

    result = await accountRepository.findByEmail(account.email)
    if (result.errors) return res.redirect('/error/500?id=find-account-after-create')
  }

  account = result.data.account[0]

  if (account.subscriptions.length == 0) {
    const subscription: ISubscription = {
      id: '',
      accountId: account.id,
      type: SUBSCRIPTION_TYPES.FREE_TRIAL,
      expiredAt: subscriptionService.generateExpiredDate(SUBSCRIPTION_TYPES.FREE_TRIAL)
    }

    result = await subscriptionRepository.create(subscription)
    if (result.errors) {
      console.dir(result.errors)
      return res.redirect('/error/500?id=create-subscription')
    }

    const subscriptionHistory: ISubscriptionHistory = {
      id: '',
      subscriptionId: result.data.insert_subscription_one.id,
      type: SUBSCRIPTION_TYPES.FREE_TRIAL,
      expiredAt: subscriptionService.generateExpiredDate(SUBSCRIPTION_TYPES.FREE_TRIAL),
      currency: 'IDR',
      price: 0
    }

    result = await subscriptionRepository.createHistory(subscriptionHistory)
    if (result.errors) {
      console.dir(result.errors)
      return res.redirect('/error/500?id=create-subscription-history')
    }
  }

  const cookieValue: any = {
    id: account.id,
    fullName: account.fullName,
    subscriptionType: account.subscriptions.length ? account.subscriptions[0].type : SUBSCRIPTION_TYPES.FREE_TRIAL,
    expiredAt: account.subscriptions.length ? account.subscriptions[0].expiredAt : undefined,
  }

  const token: string = jwt.sign(cookieValue, process.env.SECRET_HASH!)
  setCookie({ res }, cookieKey, token, {
    maxAge: 24 * 60 * 60,
    path: '/'
  })

  res.redirect('/dashboard')
}

export default OauthProviderCallback