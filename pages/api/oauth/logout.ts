import type {NextApiRequest, NextApiResponse} from "next";
import {destroyCookie} from "nookies";
import {cookieKey} from "common/constant";

const Logout = async (req: NextApiRequest, res: NextApiResponse) => {
  destroyCookie({ res }, cookieKey, { path: '/' })
  res.redirect('/')
}

export default Logout