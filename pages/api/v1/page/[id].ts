import {NextApiRequest, NextApiResponse} from "next";
import PageService from "services/page";
import AuthService from "services/auth";
import middlewareAuth from "middlewares/auth";

const api = async (req: NextApiRequest, res: NextApiResponse) => {
  const pageService: PageService = new PageService()

  try {
    let result: any = {}

    if (req.query.query_type && req.query.query_type === 'slug') {
      result = await pageService.findBySlug(req.query.id.toString())
    } else {
      const authService: AuthService = new AuthService(req.cookies)

      const pageId: string = req.query.id.toString() || ''
      const accountId: string = !req.query.query_type ? authService.getUserId() : ''

      switch (req.method?.toLowerCase()) {
        case 'put':
          result = await update(pageId, accountId, req.body)
          break
        default:
          result = await pageService.find(pageId, accountId)
      }
    }

    const {errors, data} = result
    if (errors && errors.length) {
      return res.status(500).json({ success: false, error: errors[0].message })
    }

    res.status(200).json({ success: true, data })
  } catch (e) {
    console.dir({ api: '/api/v1/page', error: e })
    res.status(500).json({ success: false, error: e })
  }
}

export default middlewareAuth(api)

const update = async (pageId: string, accountId: string, body: any) => {
  const pageService: PageService = new PageService()

  const {type}: any = body
  let result: any = {}
  switch (type.toLowerCase()) {
    case 'element-draft':
      result = await pageService.updateElementDraft(pageId, accountId, body.elementDraft)
      break
    case 'element-published':
      result = await pageService.updateElementPublished(pageId, accountId, body.elementPublished)
      break
    case 'page':
      result = await pageService.updatePageDetail(pageId, accountId, body)
      break
  }

  return result
}