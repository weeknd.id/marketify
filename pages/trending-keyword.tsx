import Layout from "./_layout";
import {Input, Modal, Table} from "antd";
import {useState} from "react";
import Fetch from "util/fetch";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import AuthService from "services/auth";
import {
  isUnauthorized,
  redirectSubscriptionExpiredServerSideProps,
  redirectUnauthorizedServerSideProps
} from "util/page-server-side";

export default function TrendingKeyword() {
  const [isPageLoading, setIsPageLoading] = useState<boolean>(false)
  const [keyword, setKeyword] = useState<string>('')
  const [dataQuery, setDataQuery] = useState<Array<any>>([])
  const [dataInterestByRegion, setDataInterestByRegion] = useState<Array<any>>([])

  const onSearch = async (e: any) => {
    setDataQuery([])
    setDataInterestByRegion([])

    const keyword: string = e.target.value
    try {
      setKeyword(keyword)
      setIsPageLoading(true)
      const result: any = await Fetch('GET', `/api/v1/trending-keyword?keyword=${keyword}`)
      if (!result.success) {
        Modal.error({ title: 'Error', content: result.error })
      } else {
        setDataQuery(result.data.query)
        setDataInterestByRegion(result.data.interestByRegion)
      }
    } catch (e) {
      Modal.error({ title: 'Error', content: 'Terjadi kesalahan. Mohon coba kembali nanti.' })
    } finally {
      setIsPageLoading(false)
    }
  }

  return (
    <Layout title={`Trending Keyword`} isLoading={isPageLoading}>
      <div className={`container mx-auto w-full md:w-4/6 py-5 px-5 md:px-0 grid grid-cols-2 gap-x-5`}>
        <div className={`col-span-2`}>
          <div className={`text-center font-bold mb-6 text-indigo-600 text-xl`}>
            Cari Trending Keyword
            {keyword != '' && <div className={`my-3 text-indigo-600 font-bold`}>&lsquo;{keyword}&rsquo;</div>}
          </div>
          <div className={`mb-5`}><Input onPressEnter={onSearch} placeholder={`Cari keyword ...`} /></div>
        </div>

        <div className={`col-span-1`}>
          <div className={`text-indigo-600 font-semibold mb-3`}>Hasil keyword yang banyak di cari</div>
          <Table columns={[
            { key: 'query', dataIndex: 'query', title: 'Keyword' },
            { key: 'value', dataIndex: 'value', title: 'Nilai' }
          ]} dataSource={dataQuery} rowKey={`query`} />
        </div>
        <div className={`col-span-1`}>
          <div className={`text-indigo-600 font-semibold mb-3`}>Minat pada wilayah berdasarkan keyword</div>
          <Table columns={[
            { key: 'geoName', dataIndex: 'geoName', title: 'Lokasi' },
            { key: 'formattedValue',
              dataIndex: 'formattedValue',
              title: 'Nilai',
              render: (formattedValue, item: any) => `${formattedValue[0] ? formattedValue[0] : ''}`
            }
          ]} dataSource={dataInterestByRegion} rowKey={`geoName`} />
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  const authService = new AuthService(cookies)

  if (isUnauthorized(cookies)) return redirectUnauthorizedServerSideProps
  if (authService.isSubscriptionExpired()) return redirectSubscriptionExpiredServerSideProps

  return {
    props: {}
  }
}