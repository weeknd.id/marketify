import {IElementProps} from "components/studio/elements/interfaces";
import {ElementGenerator} from "components/studio/elements";
import PageHead from "components/head";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {isUnauthorized} from "util/page-server-side";
import FooterIcon from "components/icon/footer";

export default function Home({ baseUrl }: any) {
  const elements: Array<IElementProps> = [
    {"id":"1","type":"HEADING","metadata":{"text":"MARKETIFY","fontSize":"text-2xl","fontWeight":"font-bold","textAlign":"text-center","paddingTop":"pt-10","marginTop":"mt-5"},"style":{"primaryColor":"indigo-600"}},
    {"id":"2ae62910-1c24-11ec-be40-8d9fcd3c1470","type":"BLOCK_CTA_1","metadata":{"type":"1","title":"Hanya dengan waktu 5 menit","description":"Kamu bisa membuat landing page sesuai kebutuhanmu dan siapkan produkmu untuk dikenal lebih banyak lagi oleh calon pelanggan barumu!","buttonText":"DAFTAR SEKARANG GRATIS","buttonUrl":"https://accounts.google.com/o/oauth2/auth?client_id=849656375737-rh47bmnuecal8dbjslmlipof1djski53.apps.googleusercontent.com&redirect_uri=" + baseUrl + "/api/oauth/google/callback&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&access_type=offline&response_type=code"},"style":{}}
  ]

  return (
    <div>
      <PageHead title={`Prototype`} />
      <main>
        <div className={`container mx-auto`}>
          {elements.map((elem, i) => (
            <ElementGenerator key={i} {...elem} />
          ))}
          <FooterIcon />
        </div>
      </main>
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  if (!isUnauthorized(cookies)) {
    return {
      redirect: {
        destination: '/dashboard',
        permanent: false
      }
    }
  }

  return {
    props: {
      baseUrl: process.env.BASE_URL
    }
  }
}