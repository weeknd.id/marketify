import {useEffect, useState} from "react";
import {IElementProps} from "components/studio/elements/interfaces";
import {ElementGenerator} from "components/studio/elements";
import Head from "next/head";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {
  isUnauthorized, redirectSubscriptionExpiredServerSideProps,
  redirectUnauthorizedServerSideProps
} from "util/page-server-side";
import AuthService from "services/auth";
import FooterIcon from "components/icon/footer";

export default function StudioPreviewPage({ isUnauthorized }: any) {
  const [elements, setElements] = useState<Array<IElementProps>>([])

  useEffect(() => {
    if (process.browser) { // @ts-ignore
      setElements(JSON.parse(localStorage.getItem('draft-elements')))
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Preview</title>
      </Head>
      <main>
        <div className={`container mx-auto`}>
          {elements.map((elem, i) => (
            <ElementGenerator key={i} {...elem} />
          ))}
          <FooterIcon />
        </div>
      </main>
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  const authService = new AuthService(cookies)

  if (isUnauthorized(cookies)) return redirectUnauthorizedServerSideProps
  if (authService.isSubscriptionExpired()) return redirectSubscriptionExpiredServerSideProps

  return {
    props: {}
  }
}